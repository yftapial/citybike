import React, { useState, useEffect } from "react";
import socketIOClient from "socket.io-client";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import {Icon} from 'leaflet'

const App = () => {
  const [response, setResponse] = useState([]);
  const endpoint = "http://127.0.0.1:4001"
  const lat = 25.8003789
  const lng = -80.1324451
  const zoom = 12
  const position = [lat,lng]

  useEffect(() => {
    const socket = socketIOClient(endpoint);
    socket.on("server", data => {
      if(data && data.network && data.network.stations) {
        setResponse(data.network.stations);
      }
    });
  }, []);
  
  return (
    <div className="map">
      <h1> City Bikes in Miami </h1>
      <Map center={position} zoom={zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {response.map( station => (
          station.free_bikes && 
          <Marker 
            key={station.id}
            position={[station.latitude, station.longitude]} 
            icon={new Icon({iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41]})} 
          >
            <Popup>
              Name: {station.name} <br /> 
              Free bikes: {station.free_bikes}
            </Popup>
          </Marker>
        ))}
      </Map>
    </div>
  );
  
}
export default App;
